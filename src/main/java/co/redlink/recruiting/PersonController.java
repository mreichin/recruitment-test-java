package co.redlink.recruiting;

/**
 * Created by mrm on 25/09/2017.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonController {

    //private static final String template = "Hello, %s!";
    //private final AtomicLong counter = new AtomicLong();
    @Autowired
    PersonRepository personRepository;

    @RequestMapping("/peoples")
    public List<Person> listPeople(@RequestParam(value="direction", defaultValue="ASC") Sort.Direction direction /*todo*/) {
        return personRepository.findAllByOrderByLastName(direction);

        //new Person(); //todo

               // new Person(counter.incrementAndGet(),
                // String.format(template, name));
    }
}
