# Recruitment Test Java - Spring Boot - Mongo - JPA
Ziel ist es, ein Backend eines "Mini" CRM anzulegen, dass folgende Funktionalität bietet:

1. CRUD einer Person mit Name, Vornamen und Geburtsdatum
2. Speichern der Daten in einer Mongo Datenbank
3. Exponierung des Services als Rest Webservice
4. Methode(n) zum Listing von Personen
	- Ordnung wählbar (Alphabetisch nach Vor-Nachnamen bzw. nach Geburtsdatum, auf oder absteigend)
	- Filter nach Alter
5. Testen des Services (Mocken des Repositories)

## Dabei soll beachtet/verwendet werden:

1. Git und Maven
2. Service Orientierted Architectur
3. Java Persistence API
4. JSON als Interchange Format

## Beispiel Interaction

```
curl -XPOST http://localhost:8080/person -d '{"firstname":"Thomas","lastname":"Kurz","birthdate":"1981-07-29"}'  -H 'Content-Type:application/json'

=> 201 CREATED: {"id":"123123","firstname":"Thomas","lastname":"Kurz","birthdate":"1981-07-29"}

curl -XGET http://localhost:8080/person/123123 -H 'Accept:application/json'

=> 200 OK: {"id":"123123","firstname":"Thomas","lastname":"Kurz","birthdate":"1981-07-29"}

curl -XGET http://localhost:8080/person?order=desc&sort=alphabet&age=24 -H 'Accept:application/json'
```

## Links

1. Aufsetzen einer Spring Boot Applikation mit Maven 
    - https://spring.io/guides/gs/spring-boot/

2. Mongo in Spring Boot
    - https://docs.mongodb.com/manual/installation/
    - https://spring.io/guides/gs/accessing-mongodb-data-rest/
    
3. Testen in Spring Boot
    - https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html